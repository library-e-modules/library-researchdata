# Library E-learning Module: ResearchData
This Lectora E-module is developed and used by Wageningen University and Research Library

The first commit of this module is based on ResearchData_20181116 version.

This module is initially created with Lectora Publisher version: 16.2.2

### License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.